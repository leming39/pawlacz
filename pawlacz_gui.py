#!/usr/bin/python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, Gio

import mutagen
from mutagen.flac import Picture
import base64

import urllib.parse
import os
import sys
import subprocess
import threading
import queue
import time
import random
import shutil
import re
import configparser
from PIL import Image

qaac = ''
if os.path.isfile('./qaac/qaac.exe'):
    qaac = "wine ./qaac/qaac.exe"
elif os.path.isfile('/usr/lib/pawlacz/qaac/qaac.exe'):
    qaac = 'wine /usr/lib/pawlacz/qaac/qaac.exe'
elif os.path.isfile('/usr/local/lib/pawlacz/qaac/qaac.exe'):
    qaac = 'wine /usr/local/lib/pawlacz/qaac/qaac.exe'

configfile = os.path.expanduser("~/.pawlacz.conf")

supported_formats = [
    {
        'name': 'OGG Vorbis', 
        'ext': '.oga', 'minquality': 0,
        'maxquality': 10,
        'qualitystep': 0.1,
        'qualitydigits': 1,
        'defaultquality': 6.0,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a libvorbis -aq /quality/ /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'libvorbis'
    },

    {
        'name': 'OGG Vorbis (44.1kHz)', 
        'ext': '.oga', 'minquality': 0,
        'maxquality': 10,
        'qualitystep': 0.1,
        'qualitydigits': 1,
        'defaultquality': 6.0,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a libvorbis -ar 44100 -aq /quality/ /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'libvorbis'
    },

    {
        'name': 'MP3 VBR',
        'ext': '.mp3',
        'minquality': 0,
        'maxquality': 9,
        'qualitystep': 1,
        'qualitydigits': 0,
        'defaultquality': 0,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a libmp3lame -aq /quality/ /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'libmp3lame'
    },
    
    {
        'name': 'MP3 CBR',
        'ext': '.mp3',
        'minquality': 128,
        'maxquality': 320,
        'qualitystep': 64,
        'qualitydigits': 0,
        'defaultquality': 320,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a libmp3lame -b:a /quality/ /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'libmp3lame'
    },

    {
        'name': 'M4A AAC (libfdk_aac)',
        'ext': '.m4a',
        'minquality': 0,
        'maxquality': 5,
        'qualitystep': 1,
        'qualitydigits': 0,
        'defaultquality': 5,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a libfdk_aac -vbr /quality/ /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'libfdk_aac'
    },

    {
        'name': 'Opus',
        'ext': '.opus',
        'minquality': 6,
        'maxquality': 510,
        'qualitystep': 1,
        'qualitydigits': 0,
        'defaultquality': 128,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a libopus -b:a /quality/k /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'libopus'
    },

    {
        'name': 'M4A AAC (qaac)',
        'ext': '.m4a',
        'minquality': 0,
        'maxquality': 127,
        'qualitystep': 1,
        'qualitydigits': 0,
        'defaultquality': 100,
        'cmd': qaac+" -V /quality/ -o /output/ /input/",
        'testcmd': qaac,
        'teststring': ''
    },

    {
        'name': 'FLAC',
        'ext': '.flac',
        'minquality': 0,
        'maxquality': 12,
        'qualitystep': 1,
        'qualitydigits': 0,
        'defaultquality': 5,
        'cmd': "ffmpeg -y -threads 1 -i /input/ -c:a flac -compression_level /quality/ /output/",
        'testcmd': 'ffmpeg -encoders',
        'teststring': 'flac'
    }
]

formats = []

q = queue.Queue()

num_worker_threads = os.cpu_count()
if num_worker_threads is None:
    num_worker_threads = 1
print("Using {} threads".format(num_worker_threads))

t_stop = threading.Event()

def fix_chars(s):
    chars = ["/", "\\", ":", "*", "?", "\"", "<", ">", "|"]
    for c in chars:
        s = s.replace(c, "_")
    return s

def check_formats():
    global formats
    for f in supported_formats:
        if test_format(f):
            print('{} supported'.format(f['name']))
            formats.append(f)
        else:
            print('{} not supported'.format(f['name']))

def test_format(f):
    cmd = f['testcmd'].split(' ')
    r = False
    try:
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True) as proc:
            while True:
                line = proc.stdout.readline()
                if line != '':
                    line = line.rstrip()
                    if f['teststring'] in line:
                        r = True
                else:
                    break
    except:
        pass
    return r

def update_song(song):
    row = song['row']
    progress = song.get('progress', None)
    status = song.get('status', None)

    if status:
        row[1] = song['status']

    if progress:
        row[2] = song['progress']

def convert_song(n, stop_event):
    while not stop_event.is_set():
        item = q.get()
        if item is None:
            break

        #item['file'][1] = 'Converting'
        GObject.idle_add(update_song, {'row': item['file'], 'status': 'Converting'})

        if not item['overwrite'] and os.path.isfile(item['out_name']):
            GObject.idle_add(update_song, {'row': item['file'], 'status': 'File exists', 'progress': 100})
            q.task_done()
            continue

        if item['out_format'] != 'copy':
            if item['out_format']['qualitydigits'] == 0:
                quality = str(round(item['quality'], None))
            else:
                quality = str(round(item['quality'], item['out_format']['qualitydigits']))

            cmd = []
            for c in item['out_format']['cmd'].split(' '):
                if c == '/input/':
                    cmd.append(item['file'][0])
                elif c == '/output/':
                    cmd.append(item['out_name'])
                elif c.startswith('/quality/'):
                    cmd.append(c.replace('/quality/', quality))
                else:
                    cmd.append(c)
            #subprocess.run(cmd)
            with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True) as proc:
                while True:
                    line = proc.stdout.readline()
                    if line != '':
                        line = line.rstrip()

                        if 'ffmpeg' in item['out_format']['cmd']:
                            if 'Duration' in line or 'time=' in line:
                                d = re.search(r'Duration: (.*?), start:', line)
                                if d is not None:
                                    a = d.group(1).split(':')
                                    duration = float(a[-1])
                                    duration += int(a[-2])*60
                                    duration += int(a[-3])*60*60
                                t = re.search(r'time=(.*?) bitrate', line)
                                if t is not None:
                                    a = t.group(1).split(':')
                                    curtime = float(a[-1])
                                    curtime += int(a[-2])*60
                                    curtime += int(a[-3])*60
                                    progress = int(curtime/duration*100)

                                    #item['file'][1] = str(progress)+"%"
                                    #item['file'][2] = progress
                                    GObject.idle_add(update_song, {'row': item['file'], 'progress': progress, 'status': str(progress)+"%"})

                        if 'qaac' in item['out_format']['cmd']:
                            p = re.search(r'\[(\d{1,3})\.\d\%\]', line)
                            if p is not None:
                                progress = int(p.group(1))
                                if progress == 100:
                                    progress = 99
                                GObject.idle_add(update_song, {'row': item['file'], 'progress': progress, 'status': str(progress)+"%"})
                    else:
                        break

            #item['file'][2] = 100
            GObject.idle_add(update_song, {'row': item['file'], 'progress': 100})

        else:
            shutil.copy(item['file'][0], item['out_name'])
            GObject.idle_add(update_song, {'row': item['file'], 'progress': 100})

        if item['cover'] is not None:
            embed_cover(item['out_name'], item['cover'])

        #item['file'].model.row_changed(item['file'].path, item['file'].iter)

        #item['file'][1] = 'Finished'
        if os.path.isfile(item['out_name']):
            GObject.idle_add(update_song, {'row': item['file'], 'status': 'Finished'})
        else:
            GObject.idle_add(update_song, {'row': item['file'], 'status': 'Failed'})

        q.task_done()
    return

def convert_stop():
    t_stop.set()
    while True:
        try:
            q.get_nowait()
            q.task_done()
        except queue.Empty:
            break


def scale_cover(src, dest, width, height):
    img = Image.open(src)
    if img.width > width or img.height > height:
        if img.width >= img.height:
            w = width
            h = round(img.height/img.width*w)
        else:
            h = height
            w = round(img.width/img.height*h)
        img = img.resize([w, h], resample=Image.LANCZOS)
        img.save(dest, quality=90)
        return True
    else:
        return False

def embed_cover(file, cover):
    if os.path.isfile(file) and os.path.isfile(cover):
        song = mutagen.File(file)
        if song is None:
            return False
        with open(cover, 'rb') as h:
            data = h.read()

        if type(song.tags) == mutagen.oggvorbis.OggVCommentDict or type(song.tags) == mutagen.oggopus.OggOpusVComment:
            picture = Picture()
            picture.data = data
            picture.type = 3
            picture.mime = "image/jpeg"
            picture_data = picture.write()
            encoded_data = base64.b64encode(picture_data)
            vcomment_value = encoded_data.decode("ascii")
            song["metadata_block_picture"] = [vcomment_value]
            song.save()

        if type(song.tags) == mutagen.mp4.MP4Tags:
            mp4cover = mutagen.mp4.MP4Cover(data, mutagen.mp4.MP4Cover.FORMAT_JPEG)
            song['covr'] = [mp4cover]
            song.save()

        if type(song.tags) == mutagen.id3.ID3:
            song.tags.setall("APIC", [mutagen.id3.APIC(data=data)])
            song.save()

class ConvertPage(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self, column_spacing=10, row_spacing=10, border_width=10)

        add_button = Gtk.Button(label="Add")
        add_button.connect("clicked", self.on_add_button_clicked)
        del_button = Gtk.Button(label="Remove")
        del_button.connect("clicked", self.on_del_button_clicked)
        del_all_button = Gtk.Button(label="Remove all")
        del_all_button.connect("clicked", self.on_del_all_button_clicked)
        clear_button = Gtk.Button(label="Clear finished")
        clear_button.connect("clicked", self.on_clear_button_clicked)

        #(1source, 2status)
        self.file_list_store = Gtk.ListStore(str, str, int)


        self.file_list = Gtk.TreeView(self.file_list_store)
        self.file_list.set_name('filelist')
        #self.file_list.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        renderer1 = Gtk.CellRendererText()
        renderer2 = Gtk.CellRendererProgress()
        column1 = Gtk.TreeViewColumn("Source", renderer1, text=0)
        #column2 = Gtk.TreeViewColumn("Status", Gtk.CellRendererText(xalign=1.0), text=1)
        column2 = Gtk.TreeViewColumn("Status", renderer2, text=1, value=2)
        self.file_list.append_column(column1)
        self.file_list.append_column(column2)

        self.file_list.connect('drag-motion', self.on_drag_motion)
        self.file_list.connect('drag-drop', self.on_drag_drop)
        self.file_list.connect('drag-data-received', self.on_drag_data_received)
        self.file_list.drag_dest_set(0, [], 0)

        file_list_window = Gtk.ScrolledWindow(expand=True)
        file_list_window.set_name('filelistwindow')
        file_list_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        file_list_window.add(self.file_list)
        file_list_window.set_min_content_height(200)


        out_dir_box = Gtk.Box(spacing=10)
        self.out_dir_entry = Gtk.Entry()
        self.out_dir_entry.set_text(os.getcwd())
        out_dir_label = Gtk.Label("Output directory: ")
        out_dir_button = Gtk.Button(label="Open")
        out_dir_button.connect("clicked", self.on_out_dir_button_clicked)
        out_dir_box.pack_start(out_dir_label, False, False, 0)
        out_dir_box.pack_start(self.out_dir_entry, True, True, 0)
        out_dir_box.pack_end(out_dir_button, False, False, 0)

        encoder_box = Gtk.Box(spacing=10)
        self.quality_button = Gtk.SpinButton()
        self.quality_button.set_numeric(True)

        self.format_combo = Gtk.ComboBoxText()
        for f in formats:
            self.format_combo.append_text(f['name'])
        self.format_combo.connect("changed", self.on_format_combo_changed)
        self.format_combo.set_active(0)
        encoder_box.pack_start(self.format_combo, False, False, 0)
        encoder_box.pack_start(self.quality_button, False, False, 0)

        pattern_box = Gtk.Box(spacing=10)
        pattern_label = Gtk.Label("File name pattern:")
        pattern_box.pack_start(pattern_label, False, False, 0)
        self.pattern_entry = Gtk.Entry()
        self.pattern_entry.set_text("{albumartist}/{year} - {album}/{track}. {title}")
        pattern_box.pack_start(self.pattern_entry, True, True, 0)

        #lossy_box = Gtk.Box(spacing=10)
        lossy_label = Gtk.Label("Force lossy files conversion")
        encoder_box.pack_start(lossy_label, False, False, 0)
        self.lossy_switch = Gtk.Switch()
        self.lossy_switch.set_active(False)
        encoder_box.pack_start(self.lossy_switch, False, False, 0)
        overwrite_label = Gtk.Label("Overwrite existing files")
        encoder_box.pack_start(overwrite_label, False, False, 0)
        self.overwrite_switch = Gtk.Switch()
        self.overwrite_switch.set_active(False)
        encoder_box.pack_start(self.overwrite_switch, False, False, 0)

        cover_box = Gtk.Box(spacing=10)
        cover_embed_label = Gtk.Label("Embed covers")
        cover_scale_label = Gtk.Label("Scale covers")
        cover_keep_label = Gtk.Label("Keep embedded covers as cover.jpg")
        self.cover_size_button = Gtk.SpinButton()
        self.cover_size_button.set_numeric(True)
        self.cover_size_button.set_increments(1, 1)
        self.cover_size_button.set_digits(0)
        self.cover_size_button.set_range(1, 9999)
        self.cover_size_button.set_value(1000)
        self.cover_scale_switch = Gtk.Switch()
        self.cover_scale_switch.connect("notify::active", self.on_cover_scale_switch_activated)
        self.cover_scale_switch.set_active(True)
        self.cover_keep_switch = Gtk.Switch()
        self.cover_keep_switch.set_active(False)
        self.cover_embed_switch = Gtk.Switch()
        self.cover_embed_switch.connect("notify::active", self.on_cover_embed_switch_activated)
        self.cover_embed_switch.set_active(True)
        cover_box.pack_start(cover_embed_label, False, False, 0)
        cover_box.pack_start(self.cover_embed_switch, False, False, 0)
        cover_box.pack_start(cover_scale_label, False, False, 0)
        cover_box.pack_start(self.cover_scale_switch, False, False, 0)
        cover_box.pack_start(self.cover_size_button, False, False, 0)
        cover_box.pack_start(cover_keep_label, False, False, 0)
        cover_box.pack_start(self.cover_keep_switch, False, False, 0)

        self.convert_button = Gtk.Button(label="Convert")
        self.convert_button.connect("clicked", self.on_convert_button_clicked)
        self.convert_button.set_name("convertbutton")

        self.attach(file_list_window, 0, 0, 5, 1)

        self.attach(add_button, 0, 1, 1, 1)
        self.attach(del_button, 1, 1, 1, 1)
        self.attach(del_all_button, 2, 1, 1, 1)
        self.attach(clear_button, 3, 1, 1, 1)
        self.attach(pattern_box, 0, 3, 5, 1)
        self.attach(encoder_box, 0, 4, 5, 1)
        self.attach(cover_box, 0, 5, 5, 1)

        self.attach(out_dir_box, 0, 6, 5, 1)
        self.attach(self.convert_button, 0, 7, 5, 1)

    def on_cover_embed_switch_activated(self, switch, gparam):
        if switch.get_active():
            #self.cover_scale_switch.set_sensitive(True)
            self.cover_keep_switch.set_sensitive(True)
        else:
            #self.cover_scale_switch.set_active(False)
            #self.cover_scale_switch.set_sensitive(False)
            self.cover_keep_switch.set_active(False)
            self.cover_keep_switch.set_sensitive(False)

    def on_cover_scale_switch_activated(self, switch, gparam):
        self.cover_size_button.set_sensitive(switch.get_active())

    def on_clear_button_clicked(self, widget):
        treeiter = self.file_list_store.get_iter_first()
        while treeiter != None:
            if self.file_list_store.get_value(treeiter, 1) == "Finished" or self.file_list_store.get_value(treeiter, 1) == "File exists":
                if self.file_list_store.remove(treeiter):
                    continue
            
            treeiter = self.file_list_store.iter_next(treeiter)

    def on_convert_button_clicked(self, widget):
        args = [
            self.file_list_store,
            formats[self.format_combo.get_active()],
            self.out_dir_entry.get_text(),
            self.pattern_entry.get_text(),
            self.quality_button.get_value(),
            self.cover_embed_switch.get_active(),
            self.cover_scale_switch.get_active(),
            self.cover_size_button.get_value_as_int(),
            self.cover_keep_switch.get_active()]
        th = threading.Thread(target=self.convert, args=args)
        th.start()

    def on_stop_button_clicked(self, widget):
        widget.set_sensitive(False)
        widget.set_label("Stopping")
        convert_stop()

    def on_format_combo_changed(self, combo):
        f = formats[combo.get_active()]
        self.quality_button.set_digits(f['qualitydigits'])
        self.quality_button.set_increments(f['qualitystep'], 1)
        self.quality_button.set_range(f['minquality'], f['maxquality'])
        self.quality_button.set_value(f['defaultquality'])

    def on_drag_motion(self, widgt, context, c, y, time):
        Gdk.drag_status(context, Gdk.DragAction.COPY, time)
        return True

    def on_drag_drop(self, widget, context, x, y, time):
        widget.drag_get_data(context, context.list_targets()[-1], time)

    def on_drag_data_received(self, widget, drag_context, x, y, data, info, time):
        if data.get_data_type() == Gdk.Atom.intern('text/uri-list', False):
            files = data.get_uris()
        elif data.get_data_type() == Gdk.Atom.intern('text/plain', False):
            d = data.get_data().decode('utf-8')
            files = d.rstrip('\n').split('\n')
        else:
            return

        for file in files:
            f = Gio.File.new_for_uri(file)
            '''p = urllib.parse.urlparse(urllib.parse.unquote(file))
            finalPath = os.path.abspath(os.path.join(p.netloc, p.path))
            self.add_file(finalPath)'''
            self.add_file(f.get_path())

    def on_add_button_clicked(self, widget):
        dialog = Gtk.FileChooserDialog("Choose files", self.get_toplevel(),
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        dialog.set_select_multiple(True)
        dialog.set_local_only(False)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            for filename in dialog.get_filenames():
                self.add_file(filename)

        dialog.destroy()

    def on_del_button_clicked(self, widget):
        (model, pathlist) = self.file_list.get_selection().get_selected_rows()
        for path in pathlist:
            model.remove(model.get_iter(path))

    def on_del_all_button_clicked(self, widget):
        self.file_list_store.clear()

    def on_out_dir_button_clicked(self, widget):
        dialog = Gtk.FileChooserDialog("Choose output folder", self.get_toplevel(),
            Gtk.FileChooserAction.SELECT_FOLDER,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.out_dir_entry.set_text(dialog.get_filename())
        dialog.destroy()

    def add_file(self, filename):
        files = []
        folders = []
        if os.path.isdir(filename):
            for x in os.scandir(filename):
                if x.is_dir():
                    folders.append(x)
                if x.is_file():
                    files.append(x)
        elif os.path.isfile(filename):
            song = mutagen.File(filename, easy=True)
            if song is None:
                return False

            it = self.file_list_store.append([filename, "Ready", 0])
            pa = self.file_list_store.get_path(it)
            self.file_list_store.row_changed(pa, it)

        files = sorted(files, key=lambda tr: tr.name)
        folders = sorted(folders, key=lambda tr: tr.name)
        for f in folders:
            if f.is_dir():
                self.add_file(f.path)

        for f in files:
            song = mutagen.File(f.path, easy=True)
            if song is None:
                return False

            it = self.file_list_store.append([f.path, "Ready", 0])
            pa = self.file_list_store.get_path(it)
            self.file_list_store.row_changed(pa, it)

    def disable_sensitive(self):
        for c in self.get_children():
            if c.get_name() != 'filelist' and c.get_name() != 'filelistwindow' and c.get_name() != 'convertbutton':
                c.set_sensitive(False)
        self.convert_button.disconnect_by_func(self.on_convert_button_clicked)
        self.convert_button.connect("clicked", self.on_stop_button_clicked)
        self.convert_button.set_label("Stop")

    def enable_sensitive(self):
        for c in self.get_children():
            c.set_sensitive(True)
        self.convert_button.disconnect_by_func(self.on_stop_button_clicked)
        self.convert_button.connect("clicked", self.on_convert_button_clicked)
        self.convert_button.set_label("Convert")
        self.convert_button.set_sensitive(True)

    def convert(self, filelist, f, out_dir, pattern, quality, cover_embed, cover_scale, cover_size, cover_keep):
        global q
        
        GObject.idle_add(self.disable_sensitive)
        l = []
        for file in filelist:
            if file[1] == 'Finished' or file[1] == 'File exists':
                continue
            song = mutagen.File(file[0], easy=True)
            if song is None:
                continue
            name = pattern.format(
                albumartist=fix_chars(song.get('albumartist', ['Unknown artist'])[0]),
                year=fix_chars(song.get('date', ['Unknown year'])[0]),
                album=fix_chars(song.get('album', ['Unknown album'])[0]),
                track=fix_chars(song.get('tracknumber', ['Unknown track'])[0]),
                title=fix_chars(song.get('title', ['Unknown title'])[0]),
                artist=fix_chars(song.get('artist', ['Unknown artist'])[0]),
                filename=fix_chars(os.path.splitext(os.path.basename(file[0]))[0])
                )
            if ((type(song) == mutagen.easymp4.EasyMP4 and song.info.codec != 'alac') or (type(song) == mutagen.mp3.EasyMP3)) and self.lossy_switch.get_active() == False:
                out_name = os.path.join(out_dir, name+os.path.splitext(file[0])[-1])
                out_format = 'copy'
            else:
                out_name = os.path.join(out_dir, name+f['ext'])
                out_format = f

            if not os.path.exists(os.path.dirname(out_name)):
                os.makedirs(os.path.dirname(out_name))

            coversrc = os.path.join(os.path.dirname(file[0]), "cover.jpg")
            coverdest = os.path.join(os.path.dirname(out_name), "cover.jpg")
            if os.path.isfile(coversrc) and not os.path.isfile(coverdest):
                shutil.copy(coversrc, coverdest)
                if cover_scale:
                    scale_cover(coverdest, coverdest, cover_size, cover_size)
                
            if os.path.isfile(coverdest) and cover_embed:
                cover = coverdest
            else:
                cover = None

            l.append({'file': file, 'out_format': out_format, 'out_name': out_name, 'quality': quality, 'cover': cover,
                'overwrite': self.overwrite_switch.get_active()})
        
        t_stop.clear()
        q = queue.Queue()
        while True:
            try:
                q.get_nowait()
                q.task_done()
            except queue.Empty:
                break
        threads = []
        for i in range(num_worker_threads):
            t = threading.Thread(target=convert_song, args=[i, t_stop])
            t.start()
            threads.append(t)

        for item in l:
            q.put(item)
        q.join()
        if not t_stop.is_set():
            for i in range(num_worker_threads):
                q.put(None)
        for t in threads:
            t.join()


        if cover_embed and not cover_keep:
            for file in l:
                if file['cover'] is not None and os.path.isfile(file['cover']):
                    os.unlink(file['cover'])

        GObject.idle_add(self.enable_sensitive)
        #time.sleep(0.05)
        #filelist.clear()
        return

    def save_config(self):
        config = configparser.ConfigParser()
        config['Convert'] = {}
        convertconfig = config['Convert']
        convertconfig['pattern'] = self.pattern_entry.get_text()
        convertconfig['format'] = str(self.format_combo.get_active())
        convertconfig['quality'] = str(self.quality_button.get_value())
        convertconfig['forcelossy'] = str(self.lossy_switch.get_active())
        convertconfig['overwrite'] = str(self.overwrite_switch.get_active())
        convertconfig['embedcovers'] = str(self.cover_embed_switch.get_active())
        convertconfig['scalecovers'] = str(self.cover_scale_switch.get_active())
        convertconfig['coversize'] = str(self.cover_size_button.get_value_as_int())
        convertconfig['keepcovers'] = str(self.cover_keep_switch.get_active())
        convertconfig['output'] = self.out_dir_entry.get_text()
        with open(configfile, 'w') as cf:
            config.write(cf)

    def load_config(self):
        if os.path.isfile(configfile):
            config = configparser.ConfigParser()
            config.read(configfile)
            if 'Convert' in config:
                convertconfig = config['Convert']
                self.pattern_entry.set_text(convertconfig.get('pattern', '{albumartist}/{year} - {album}/{track}. {title}'))
                self.format_combo.set_active(int(convertconfig.get('format', '0')))
                self.quality_button.set_value(float(convertconfig.get('quality', str(formats[self.format_combo.get_active()]['defaultquality']))))
                self.lossy_switch.set_active(convertconfig.getboolean('forcelossy', False))
                self.overwrite_switch.set_active(convertconfig.getboolean('overwrite', False))
                self.cover_embed_switch.set_active(convertconfig.getboolean('embedcovers', True))
                self.cover_scale_switch.set_active(convertconfig.getboolean('scalecovers', True))
                self.cover_size_button.set_value(int(convertconfig.get('coversize', 1000)))
                self.cover_keep_switch.set_active(convertconfig.getboolean('keepcovers', False))
                self.out_dir_entry.set_text(convertconfig.get('output', os.getcwd()))


class CoversPage(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self, column_spacing=10, row_spacing=10, border_width=10)
        self.add(Gtk.Label('Embed/extract covers'))


class PawlaczWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Pawlacz Converter")
        self.connect("delete-event", self.on_delete)

        

        notebook = Gtk.Notebook()
        self.add(notebook)


        self.convert_page = ConvertPage()
        covers_page = CoversPage()

        notebook.append_page(self.convert_page, Gtk.Label("Convert"))
        notebook.append_page(covers_page, Gtk.Label("Covers"))

        self.convert_page.load_config()

    def on_delete(self, widget, event):
        convert_stop()
        self.convert_page.save_config()
        Gtk.main_quit(widget, event)
    

def main():
    check_formats()

    if len(formats) < 1:
        dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "No supported formats")
        dialog.run()
        dialog.destroy()
        sys.exit()

    win = PawlaczWindow()



    win.show_all()
    Gtk.main()

if __name__ == '__main__':
    main()
